<!DOCTYPE html>
<!-- saved from url=(0105)http://staging.doku.com/Suite/ProcessPayment?MALLID=3443&CHAINMERCHANT=0&INV=085221597305SHP148652738110# -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>DOKU Payment Page - Bank Transfer</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
 <![endif]-->

<link rel="stylesheet" type="text/css" href="./virtual_files/default.min.css">


</head>

<body>
<table width="800px">
	<tr>
		<td style="text-align: center;">
			<img src="{{URL::asset('/img/email/jaringanatm.jpg')}}" alt="">
		</td>		
	</tr>
	<tr><td style="text-align: center;font-size: 14px;padding-top: 30px;color:#11a88a">Kode Bayar</td></tr>
	<tr><td style="text-align: center;font-size: 27px;color:#11a88a">8965095200000026</td></tr>	
	<tr><td style="font-size: 16px;padding-top: 30px;padding-left: 20px;">Jumlah :</td></tr>		
	<tr><td style="font-size: 30px;padding-left: 20px;color:#b90a29">IDR 129000.00 </td></tr>		
	<tr><td style="font-size: 16px;padding-top: 30px;padding-left: 20px;">Nomor Invoice :</td></tr>		
	<tr><td style="font-size: 16px;padding-left: 20px;">085221597305SHP148652738110 </td></tr>		
	<tr><td style="font-size: 16px;padding-top: 30px;padding-left: 20px;color: #0000ff;font-weight: bold;">Cara membayar di ATM</td></tr>
	<tr><td style="font-size: 16px;padding-top: 30px;padding-left: 18px;">Cara membayar di ATM</td></tr>
	<tr><td style="font-size: 16px;padding-top: 15px;padding-left: 18px;">1. Masukkan PIN</td></tr>
	<tr><td style="font-size: 16px;padding-top: 15px;padding-left: 18px;">2. Pilih "Transfer". Apabila menggunakan ATM BCA, pilih "Transaksi lainnya" lalu "Transfer</td></tr>
	<tr><td style="font-size: 16px;padding-top: 15px;padding-left: 18px;">3. Pilih "Ke Rek Bank Lain"</td></tr>
	<tr><td style="font-size: 16px;padding-top: 15px;padding-left: 18px;">4. Masukkan Kode Bank Permata (013) diikuti 16 digit kode bayar 8965095200000026 sebagai rekening tujuan, kemudian tekan "Benar""</td></tr>
	<tr><td style="font-size: 16px;padding-top: 15px;padding-left: 18px;">5. Masukkan Jumlah pembayaran sesuai dengan yang ditagihkan (Jumlah yang ditransfer harus sama persis, tidak boleh lebih dan kurang). Jumlah nominal yang tidak sesuai dengan tagihan akan menyebabkan transaksi gagal</td></tr>
	<tr><td style="font-size: 16px;padding-top: 15px;padding-left: 18px;">6. Muncul Layar Konfirmasi Transfer yang berisi nomor rekening tujuan Bank Permata dan Nama beserta jumlah yang dibayar, jika sudah benar, Tekan "Benar"</td></tr>
	<tr><td style="font-size: 16px;padding-top: 15px;padding-left: 18px;">7. Selesai</td></tr>
	<tr><td style="font-size: 16px;padding-top: 30px;padding-left: 20px;color: #0000ff;font-weight: bold;">Cara membayar di Intenet Bangking</td></tr>
	<tr><td style="font-size: 16px;padding-top: 15px;padding-left: 20px;color: #b90a29;">Keterangan: Pembayaran tidak bisa dilakukan di Internet Banking BCA (KlikBCA)</td></tr>
	<tr><td style="font-size: 16px;padding-top: 15px;padding-left: 18px;">1. Login ke dalam akun Internet Banking</td></tr>
	<tr><td style="font-size: 16px;padding-top: 15px;padding-left: 18px;">2. Pilih "Transfer" dan pilih "Bank Lainnya". Pilih Bank Permata (013) sebagai rekening tujuan</td></tr>
	<tr><td style="font-size: 16px;padding-top: 15px;padding-left: 18px;">3. Masukkan jumlah pembayaran sesuai dengan yang di tagihkan</td></tr>
	<tr><td style="font-size: 16px;padding-top: 15px;padding-left: 18px;">4. Isi nomor rekening tujuan dengan 16 digit kode pembayaran 8965095200000026</td></tr>
	<tr><td style="font-size: 16px;padding-top: 15px;padding-left: 18px;">5. Muncul layar konfirmasi Transfer yang berisi nomor rekening tujuan Bank Permata dan Nama beserta jumlah yang dibayar. Jika sudah benar, tekan "Benar"</td></tr>
	<tr><td style="font-size: 16px;padding-top: 15px;padding-left: 18px;">6. Selesai</td></tr>
	<tr><td style="text-align: center; font-size: 15px;padding-top: 50px;">Copyright DOKU 2016</td></tr>
</table>	

</body></html>