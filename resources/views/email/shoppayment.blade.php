<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE">
    <title>PopShop Invoice</title>
</head>
<body style="font-family:arial">
<table  cellspacing="0" cellpadding="0">
	<tbody>
		<tr>
			<td><img src="{{URL::asset('/img/email/logo-popbox.png')}}"/></td>
			<td style="width: 56%"></td>
			<td><a href="www.popbox.asia" style="text-decoration: none;font-size: 30px;color: #000;">www.popbox.asia</a></td>
		</tr>
		<tr><td colspan="3" style="border-top: 1px solid #000;"></td></tr>
		<tr><td colspan="3" style="text-align: center;"><img src="{{URL::asset('/img/email/logo-deliver.png')}}"/></td></tr>
		<tr><td colspan="3">
				Hai, {{$member_name}}<br/><br/>
				Terima kasih! Pembayaran untuk order {{$invoice_id}} sudah kami terima. Barang anda sedang diproses!<br/><br/>
				Ingin mengetahui keberadaan produk yang kamu pesan?<br/><br/>
				<a href="{{config('config.domain_production')}}/paybyqr/invoicetransfer?transid={{$transid}}" style="text-decoration: none;"><div style="background: red; text-align: center;color: #FFF;padding:5px;width: 20%; border-radius: 10px;">
					Cek Pesanan
				</div></a><br/><br/>
				<div style="font-size: 30px;padding:30px 0px">PopBox cepat, mudah, dan nyaman</div>
			</td>
		</tr>
		<tr><td colspan="3" style="text-align: center;font-size: 25px">
			Apabila ada pertanyaan, kami siap membantu anda<br/>
			Silahkan hubungi kami melalui			
		</tr>
		<tr><td colspan="3" style="text-align: center;">
			<span style="font-size: 30px">info@popbox.asia</span> atau <span style="font-size: 30px">021 - 29022537</span>
		</tr>		
		<tr><td>
			&nbsp;
			</td>
		</tr>	
		<tr><td>
			&nbsp;
			</td>
		</tr>		
		<tr style="background: #5d5d5e;">
			<td colspan="3" style="text-align: center;height: 97px">
				<a href="https://www.facebook.com/pboxasia"><img src="{{URL::asset('/img/email/logo-fb1.png')}}" style="margin-right: 10%"></a>
				<a href="https://instagram.com/popbox_asia"><img src="{{URL::asset('/img/email/logo-ins.png')}}" style="margin-right: 10%"></a>
				<a href="https://twitter.com/popbox_asia"><img src="{{URL::asset('/img/email/logo-tw.png')}}"></a>
			</td>
		</tr>
	</tbody> 
</table>
</body>
</html>