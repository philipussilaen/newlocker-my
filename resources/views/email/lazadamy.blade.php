<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<p>Hai Admin, </p>
		<p> Please process these Lazada Return Parcels  </p>
		<p>
		<table style="border-spacing: 0;border: 1px solid black;">	
			<tr style="background:#f9f9f9;">
				<td style="border-spacing: 0;border: 1px solid black;">Tracking No</td>
				<td style="border-spacing: 0;border: 1px solid black;">Phone Number</td>
				<td style="border-spacing: 0;border: 1px solid black;">Locker Name</td>
				<td style="border-spacing: 0;border: 1px solid black;">Locker Size</td>
				<td style="border-spacing: 0;border: 1px solid black;">Locker Name</td>
				<td style="border-spacing: 0;border: 1px solid black;">Storetime</td>
			</tr>
			@foreach ($lzmy as $lz)
			<tr>
				<td style="border-spacing: 0;border-left: 1px solid black;border-right: 1px solid black;">{{ $lz->tracking_no }}</td>
				<td style="border-spacing: 0;border-left: 1px solid black;border-right: 1px solid black;">{{ $lz->phone_number }}</td>
				<td style="border-spacing: 0;border-left: 1px solid black;border-right: 1px solid black;">{{ $lz->locker_name }}</td>
				<td style="border-spacing: 0;border-left: 1px solid black;border-right: 1px solid black;">{{ $lz->locker_size }}</td>
				<td style="border-spacing: 0;border-left: 1px solid black;border-right: 1px solid black;">{{ $lz->locker_number }}</td>
				<td style="border-spacing: 0;border-left: 1px solid black;border-right: 1px solid black;">{{ $lz->storetime }}</td>
			</tr>
			@endforeach
		</table>		
	</body>
</html>