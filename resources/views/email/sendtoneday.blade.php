<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<p>Dear Admin, </p>
		<p> Please send this parcel to destination address as follows  :  <br/>
			Invoice No / Barcode : {{ $detail['order_no'] }} <br/>
			Pickup Address : {{ $detail['pick_up_addr_1']  }} {{ $detail['pick_up_addr_2']  }} <br />
			Recipient Name : {{ $detail['deliver_to']  }}<br />
			Recipient Phone  : {{ $detail['phone']  }}<br />
			Recipient Address : {{ $detail['addr_1']  }} {{ $detail['addr_2']  }} <br/>
			Recipient Zip Code :  {{ $detail['postal_code']  }} <br/>
			Instructions :   {{ $detail['instructions'] }} <br/>
		</p>
	
			
	</body>
</html>