<!DOCTYPE html>
<html lang="en-US">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<p>Hai Admin, </p>
		<p> Ada Transfer dari {{ $phone }} ke rekening {{ $bank_transfer_dest }} yang harus segera diproses  </p>
		
		Data detail : <br>
		Nama layanan : {{ $service_name }}
		No. Handhpone : {{ $phone }} <br>
		No. Invoice : {{ $invoice_no }} <br>
		Bank Tujuan Transfer : {{ $bank_transfer_dest }} <br>
		Nama Pengirim : {{ $sender_bank_name }} <br>
		No. Rekening Pengirim : {{ $sender_bank_account_number }} <br>
		Jumlah Transfer  : {{ $transfer_amount }} <br>
		Tanggal transfer : {{ $payment_date }} <br>
		Foto Transfer : <?php echo (empty($image)) ? "-" : "<a href='".config('config.api_host')."/img/transfer/".$image."'>Clik untuk melihat bukti transfer</a>"; ?>				
	</body>
</html>