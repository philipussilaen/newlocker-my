<p>Pelangan yang terhormat,</p>
<p>
    Order Anda berhasil dibuat, silakan melakukan pembayaran kepada partner kami. <br>
    Berikut rincian order Anda :
</p>

<p>
    No Order :  <strong>{{ $data['invoiceId'] }}</strong>
</p>
<p>
    Detail Lokasi : <br>
    {{ $data['locker'] }}
    @if(!empty($lockerData))
        {{ empty($lockerData['address_2'])? null :  $lockerData['address_2'] }} <br>
        {{ empty($lockerData['address'])? null : $lockerData['address'] }} <br>
        {{ empty($lockerData['operational_hours'])? null :  $lockerData['operational_hours'] }}
    @endif
</p>
<p>
    Nama : {{ $data['nama'] }} <br>
    Email : {{ $data['email'] }}
</p>
<p>
    Langkah-langkah menyimpan barang Anda di loker :
    <ol>
    <li>Datang ke lokasi Loker</li>
    <li>Pilih opsi bahasa</li>
    <li>Pilih opsi LAUNDRY</li>
    <li>Masukkan nomor order yang Anda dapat dari SMS atau email ini</li>
    <li>Jangan lupa menutup kembali pintu loker</li>
    <li>Order Anda akan kami proses</li>
    </ol>
</p>

<p>
    Terima kasih telah menggunakan layanan PopBox <br>
    Informasi lengkap mengenai lokasi E-Locker/PopBox dapat dilihat di <a href="www.popbox.asia/locations" target="_blank">www.popbox.asia/locations/</a>
</p>
<br><br>
<p>
    Salam Hangat, <br>
    <br>
    PopBox Asia <br>
    Grand Slipi Tower Unit 21J <br>
    Jl. Letjen S. Parman Kav 22-24 <br>
    Jakarta Barat 11480 <br>
    Tlp.021-29022537/38 <br>
    <a href="www.popbox.asia">www.popbox.asia</a>
</p>
