<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddResponseColumnToTableTbSepulsaTransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tb_sepulsa_transaction', function (Blueprint $table) {
            $table->text('response_data')->after('product_data')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tb_sepulsa_transaction', function (Blueprint $table) {
            $table->dropColumn('response_data');
        });
    }
}
